$(document).ready(function() {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gOrderObject = [];
    var gNewOrderObject = [];
    const gCOLUMN_ORDER_ID = 0;
    const gCOLUMN_KICH_CO_COMBO = 1;
    const gCOLUMN_LOAI_PIZZA = 2;
    const gCOLUMN_NUOC_UONG = 3;
    const gCOLUMN_THANH_TIEN = 4;
    const gCOLUMN_HO_VA_TEN = 5;
    const gCOLUMN_SO_DIEN_THOAI = 6;
    const gCOLUMN_TRANG_THAI = 7;
    const gACTION_COL = 8;
    var gOrderObj = {
        kichCo: "",
        duongKinh: 0,
        suon: 0,
        salad: 0,
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: 0,
        hoTen: "",
        thanhTien: 0,
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    }
    var gDataRow;
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    var gPizzaData = [
        { "kichCo": "S", "duongKinh": 20, "suon": 2, "salad": 200, "soLuongNuoc": 2, "thanhTien": 150000 },
        { "kichCo": "M", "duongKinh": 25, "suon": 4, "salad": 300, "soLuongNuoc": 3, "thanhTien": 200000 },
        { "kichCo": "L", "duongKinh": 30, "suon": 8, "salad": 500, "soLuongNuoc": 4, "thanhTien": 250000 },
    ];
    const gCOLUMN_DATA = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    // định nghĩa table  - chưa có data
    var gOrderTable = $("#order-table").DataTable({
        // Khai báo các cột của datatable
        "columns": [
            { "data": gCOLUMN_DATA[gCOLUMN_ORDER_ID] },
            { "data": gCOLUMN_DATA[gCOLUMN_KICH_CO_COMBO] },
            { "data": gCOLUMN_DATA[gCOLUMN_LOAI_PIZZA] },
            { "data": gCOLUMN_DATA[gCOLUMN_NUOC_UONG] },
            { "data": gCOLUMN_DATA[gCOLUMN_THANH_TIEN] },
            { "data": gCOLUMN_DATA[gCOLUMN_HO_VA_TEN] },
            { "data": gCOLUMN_DATA[gCOLUMN_SO_DIEN_THOAI] },
            { "data": gCOLUMN_DATA[gCOLUMN_TRANG_THAI] },
            { "data": gCOLUMN_DATA[gACTION_COL] }
        ],
        // Ghi đè nội dung của cột action, chuyển thành button chi tiết
        "columnDefs": [{
            targets: gACTION_COL,
            className: "text-center",
            defaultContent: `
            <button class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Edit" id = "btn-edit"><i class="fas fa-edit" text-success style = "cursor:pointer; color: green"></i></button>
            <button class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Delete" id = "btn-delete"><i class="fas fa-trash-alt" text-success style = "cursor:pointer; color: red"></i></button>
            `
        }]
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    // sự kiện load trang
    onPageLoading();
    // sự kiện khi nhấn nút lọc dữ liệu
    $("#btn-filter-order").on("click", function() {
            onBtnFilterOrderClick();
        })
        // sự kiện khi nhấn nút Thêm đơn hàng
    $("#btn-add-order").on("click", function() {
            onBtnAddNewOrderClick();
            $("#insert-order-modal")
                .find("input,textarea,select")
                .val('')
                .end()
            $("#input-create-trang-thai").val("open");
            $("#input-create-trang-thai").prop("disabled");
        })
        // sự kiện khi nhấn chọn kích cỡ pizza
    $("#input-create-kich-co").on("change", function() {
            onBtnChangeKichCoSelect();
        })
        // sự kiến khi nhấn nút Tạo đơn hàng mới
    $("#btn-insert-order").on("click", function() {
            onBtnInsertNewOrderClick();

        })
        // sự kiện khi nhấn biểu tượng Sửa
    $(document).on("click", "#btn-edit", function() {
            onBtnEditOrderClick(this);
            $("#update-order-modal").modal("show");
            $("#update-order-modal")
                .find("input,textarea,select")
                .val('')
                .end()
            loadDataToModal(gDataRow);
        })
        // sự kiện khi nhấn nút cập nhật đơn hàng
    $("#btn-update-order").click(function() {
            console.log("nút update được ấn")
            gDataRow.trangThai = $("#select-update-trang-thai").val();
            var vObjectRequest = {
                trangThai: gDataRow.trangThai //3 trang thai open, confirmed, cancel
            }
            console.log(gDataRow)
            callApiToUpdateAndCancel(gDataRow.id, vObjectRequest)
                //gọi API lấy danh sách user
            callApiToGetAllOrder();
            //load dữ liệu vào bảng
            loadDataToTable(gOrderObject);
            // xóa trắng dữ liệu trên modal\ 
            // ẩn modal form
            $("#update-order-modal").modal("hide");
        })
        // hàm xử lý sự kiện Xóa
    $("#order-table").on("click", "#btn-delete", function() {
            onBtnDeleteClick(this);
        })
        // hàm xử lý sự kiện Xác nhận xóa đơn hàng
    $("#btn-confirm-delete-order").on("click", function() {
        onBtnDeleteConfirmOrderClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm chạy khi trang được load
    function onPageLoading() {
        // lấy data từ server
        callApiToGetAllOrder();
        loadDataToTable(gOrderObject);
    }
    // hàm xử lý sự kiện khi nhấn nút sửa
    function onBtnEditOrderClick(paramButtonElement) {
        "use strict";
        console.log("Nút Sửa được nhấn!");
        var vRowClick = $(paramButtonElement).closest("tr"); // xác định tr chứa nút bấm được click
        var vTable = $("#order-table").DataTable(); // tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); // Dữ liệu của hàng dữ liệu chứa nút bấm được click
        gDataRow = vDataRow;
    }
    // hàm xử lý sự kiện khi nhấn nút lọc
    function onBtnFilterOrderClick() {
        //B1. Khai báo biên và thu thập dữ liệu
        var vFilterObj = {
            trangThai: "",
            loaiPizza: ""
        }
        getFilterData(vFilterObj);
        //B2. Kiểm tra dữ liệu (bỏ qua)
        //B3. Lọc dữ liệu và hiển thị kết quả
        var filteredOrder = filterOrder(vFilterObj);
        loadDataToTable(filteredOrder);
    }
    // hàm xử lý sự kiện khi nhấn nút thêm đơn hàng
    function onBtnAddNewOrderClick() {
        "use strict";
        $("#insert-order-modal").modal("show");
        var vDrinkList = getDrinkList();
        loadDrinkToModal(vDrinkList);
    }
    // hàm xử lý sự kiện khi nhấn nút Thêm đơn hàng mới
    function onBtnInsertNewOrderClick() {
        "use strict";
        //khai báo đối tượng chứa user data
        getOrderDataCreate(gOrderObj);
        //B2: Validate Insert
        var vIsValidate = validateData(gOrderObj);
        if (vIsValidate) {
            // B3: Insert user
            callApiToCreateOrder(gOrderObj);
            //B4: xử lý hiển thị
            //gọi API lấy danh sách user
            callApiToGetAllOrder();
            //load dữ liệu vào bảng
            loadDataToTable(gOrderObject);
            // xóa trắng dữ liệu trên modal\ 
            // ẩn modal form
            $("#insert-order-modal").modal("hide");
            //}
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    // hàm call API lấy tất cả đơn hàng (để đổ vào Data Table)
    function callApiToGetAllOrder() {
        "use strict";
        $.ajax({
            url: gBASE_URL,
            async: false,
            type: "GET",
            dataType: 'json',
            success: function(responseObject) {
                gOrderObject = responseObject;
                console.log(gOrderObject);
            },
            error: function(error) {
                console.assert(error.responseText);
            }
        });
    }
    //hàm lọc dữ liệu
    function filterOrder(paramFilter) {
        var result = [];
        result = gOrderObject.filter(function(order) {
            return ((paramFilter.trangThai == "" || order.trangThai.toLowerCase() == paramFilter.trangThai.toLowerCase()) &&
                (paramFilter.loaiPizza == "" || order.loaiPizza.toLowerCase() == paramFilter.loaiPizza.toLowerCase()))
        })
        return result;
    }
    // hàm lấy dữ liệu được chọn tại các ô điều kiện lọc
    function getFilterData(paramFilterObj) {
        paramFilterObj.trangThai = $("#select-order-status option:selected").val();
        paramFilterObj.loaiPizza = $("#select-pizza-type option:selected").val();
    }
    // hàm xử lý sự kiện điền thông tin vào các ô đường kính, sườn, salad, số lượng nước, thành tiền khi chọn kích cỡ pizza
    function onBtnChangeKichCoSelect() {
        "use strict";
        var vKichCo = $("#input-create-kich-co").val();
        var vDataResult = getDataByKichCo(vKichCo);
        console.log(vDataResult);
        if (vDataResult.length > 0) {
            // thêm dữ liệu vào các trường đường kính, sườn, salad, số lượng nước, thành tiền dựa vào kích cỡ pizza được chọn
            addDataToForm('#input-create-duong-kinh', vDataResult[0]);
            addDataToForm('#input-create-suon', vDataResult[1]);
            addDataToForm('#input-create-salad', vDataResult[2])
            addDataToForm('#input-create-so-luong-nuoc', vDataResult[3])
            addDataToForm('#input-create-thanh-tien', vDataResult[4])
        } else {
            //thêm thuộc tính disabled cho các trường đường kính, sườn, salad, số lượng nước, thành tiền khi không có kích cỡ nào được chọn
            inputDisabled('#input-create-duong-kinh');
            inputDisabled('#input-create-suon');
            inputDisabled('#input-create-salad');
            inputDisabled('#input-create-so-luong-nuoc');
            inputDisabled('#input-create-thanh-tien');
        }
    }
    // hàm thêm thuộc tính disabled khi không có kích cỡ nào được chọn
    function inputDisabled(paramData) {
        $(paramData).attr("disabled", "disabled"); // thêm thuộc tính disabled khi không có kích cỡ nào được chọn
    }
    // hàm thêm dữ liệu vào các trường đường kính, sườn, salad, số lượng nước, thành tiền dựa vào kích cỡ pizza được chọn
    function addDataToForm(paramId, paramData) {
        $(paramId).removeAttr("disabled"); // Gỡ bỏ thuộc tính disabled
        $(paramId).val(paramData);
        $(paramId).attr("disabled", "disabled");
    }
    // hàm lấy dữ liệu các trường Đường kính, sườn, salad, số lượng nước, thành tiền dựa vào kích cỡ pizza được chọn
    function getDataByKichCo(paramKichCo) {
        var vKichCo = false;
        var vDataResult = [
            { duongKinh: 0 },
            { suon: 0 },
            { salad: 0 },
            { soLuongNuoc: 0 },
            { thanhTien: 0 }
        ]
        var vIterator = 0;
        while (!vKichCo && vIterator < gPizzaData.length) {
            if (gPizzaData[vIterator].kichCo === paramKichCo) {
                vKichCo = true;
                vDataResult[0] = gPizzaData[vIterator].duongKinh;
                vDataResult[1] = gPizzaData[vIterator].suon;
                vDataResult[2] = gPizzaData[vIterator].salad;
                vDataResult[3] = gPizzaData[vIterator].soLuongNuoc;
                vDataResult[4] = gPizzaData[vIterator].thanhTien;
            } else {
                vIterator++;
            }
        }
        return vDataResult;
    }
    // hàm thu thập dữ liệu đơn hàng được thêm mới trên modal create order
    function getOrderDataCreate(paramOrderObj) {
        "use strict";
        paramOrderObj.kichCo = $("#input-create-kich-co").val();
        paramOrderObj.duongKinh = $("#input-create-duong-kinh").val();
        paramOrderObj.suon = $("#input-create-suon").val();
        paramOrderObj.salad = $("#input-create-salad").val();
        paramOrderObj.loaiPizza = $("#select-loai-pizza option:selected").val();
        paramOrderObj.idVourcher = $("#input-create-id-voucher").val();
        paramOrderObj.idLoaiNuocUong = $("#select-id-loai-nuoc-uong option:selected").val();
        paramOrderObj.soLuongNuoc = $("#input-create-so-luong-nuoc").val();
        paramOrderObj.hoTen = $("#input-create-ho-ten").val();
        paramOrderObj.thanhTien = $("#input-create-thanh-tien").val();
        paramOrderObj.email = $("#input-create-email").val();
        paramOrderObj.soDienThoai = $("#input-create-so-dien-thoai").val();
        paramOrderObj.diaChi = $("#input-create-dia-chi").val();
        paramOrderObj.loiNhan = $("#input-create-loi-nhan").val();
    }
    // hàm call Api để tạo đơn hàng mới
    function callApiToCreateOrder(paramObjectRequest) {
        "use strict";
        $.ajax({
            url: gBASE_URL,
            async: false,
            type: "POST",
            data: JSON.stringify(paramObjectRequest),
            contentType: "application/json;charset=UTF-8",
            success: (responseObject) => {
                gNewOrderObject = responseObject;
                console.log(gNewOrderObject);
                alert("Thêm đơn hàng mới thành công!!");
            },
            error: (error) => {
                console.assert(error.responseText);
            }
        });
    }
    //hàm validate dữ liệu
    function validateData(paramData) {
        "use strict";
        if (paramData.kichCo == "") {
            alert("Bạn cần chọn kích cỡ Pizza!!!")
            return false;
        }
        if (paramData.loaiPizza == "") {
            alert("Bạn cần chọn Loại Pizza!!!")
            return false;
        }
        if (paramData.idLoaiNuocUong == "") {
            alert("Bạn cần chọn Loại nước uống!!!")
            return false;
        }
        if (paramData.hoTen == "") {
            alert("Bạn cần nhập họ tên!!!")
            return false;
        }
        if (paramData.email.length > 0) {
            var vCheckEmail = validateEmail(paramData.email);
            if (vCheckEmail === false) {
                return false;
            }
        }
        if (paramData.soDienThoai == "") {
            alert("Bạn cần nhập số điện thoại!!!")
            return false;
        }
        if (paramData.diaChi == "") {
            alert("Bạn cần nhập địa chỉ!!!")
            return false;
        }

        return true;
    }
    // hàm kiểm tra email
    function validateEmail(paramEmail) {
        "use strict";
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var vCheckEmail = true;
        if (!mailformat.test(paramEmail)) {
            alert("Email chưa hợp lệ");
            vCheckEmail = false;
        }
        return vCheckEmail;
    }
    // hàm call API để cập nhật hoặc Hủy đơn hàng
    function callApiToUpdateAndCancel(paramId, paramObjectRequest) {
        "use strict";
        $.ajax({
            url: gBASE_URL + "/" + paramId,
            async: false,
            type: "PUT",
            data: JSON.stringify(paramObjectRequest),
            contentType: "application/json;charset=UTF-8",
            success: (responseObject) => {
                alert("cập nhật trạng thái " + responseObject.trangThai + " thành công");
            },
            error: (error) => {
                console.assert(error.responseText);
            }
        })
    }
    // load data to table
    function loadDataToTable(paramResponseObject) {
        //Xóa toàn bộ dữ liệu đang có của bảng
        gOrderTable.clear();
        //Cập nhật data cho bảng 
        gOrderTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị bảng
        gOrderTable.draw();
    }
    // hàm load dữ liệu từ dòng được chọn vào modal Sửa đơn hàng
    function loadDataToModal(paramOrderObject) {
        "use strict";
        $("#input-update-kich-co").val(paramOrderObject.kichCo);
        $("#input-update-duong-kinh").val(paramOrderObject.duongKinh);
        $("#input-update-suon").val(paramOrderObject.suon);
        $("#select-update-id-loai-nuoc-uong option:selected").text(paramOrderObject.idLoaiNuocUong);
        $("#input-update-so-luong-nuoc").val(paramOrderObject.soLuongNuoc);
        $("#input-update-id-voucher").val(paramOrderObject.idVourcher);
        $("#select-update-loai-pizza option:selected").text(paramOrderObject.loaiPizza);
        $("#input-update-salad").val(paramOrderObject.salad);
        $("#input-update-email").val(paramOrderObject.email);
        $("#input-update-ho-ten").val(paramOrderObject.hoTen);
        $("#input-update-so-dien-thoai").val(paramOrderObject.soDienThoai);
        $("#input-update-dia-chi").val(paramOrderObject.diaChi);
        $("#input-update-loi-nhan").val(paramOrderObject.loiNhan);
        $("#select-update-trang-thai option:selected").text(paramOrderObject.trangThai);
        $("#input-update-thanh-tien").val(paramOrderObject.thanhTien);

    }
    // hàm lấy danh sách nước uống (đổ vào ô select)
    function getDrinkList() {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: 'json',
            success: (responseObject) => {
                console.log(responseObject);
                loadDrinkToModal(responseObject);
            },
            error: (error) => {
                console.assert(error.responseText);
            }
        })
    }
    // hàm load danh sách nước uống vào ô select ở modal
    function loadDrinkToModal(paramDrinkObject) {
        "use strict";
        $.each(paramDrinkObject, (i, item) => {
            $("#select-id-loai-nuoc-uong").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        })
    }

    function onBtnDeleteClick(paramDelete) {
        "use strict";
        console.log('Nút delete được ấn !!!')
        var vCurrentRow = $(paramDelete).closest('tr');
        var vTable = $('#order-table').DataTable();
        gDataRow = vTable.row(vCurrentRow).data();
        console.log(gDataRow)
        $('#delete-confirm-modal').modal('show');
    }

    function onBtnDeleteConfirmOrderClick() {
        console.log('Nút confirm delete trên modal delete được ấn !!!')
        deleteUserApi(gDataRow.id);
        callApiToGetAllOrder();
        loadDataToTable(gOrderObject);
        $('#delete-confirm-modal').modal('hide');

    }



    //call api để xoá dữ đơn hàng
    function deleteUserApi(paramUserId){
        "use strict";
        $.ajax ({
              async: false,
              url: gBASE_URL + "/"  + paramUserId,
              type: "DELETE",
              data: 'json',
              success: function(res){
                  console.log(res);
                  alert('Xoá đơn hàng ' + gDataRow.orderId + ' thành công !!!')
              },
              error: function(error){
                  alert(error.responseText);
              }
          })
      }

})